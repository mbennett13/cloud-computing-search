#from Tkinter import *
import os

from flask import Flask
#class Application(Frame):
app = Flask(__name__)

@app.route('/')
def home():
    target = os.environ.get('TARGET', 'database')
    return 'Search {}!\n'.format(target)
 #def __init__(self,master):
#    self.master = master
#    self.create_widgets()



 #def create_widgets(self):
#    btn1 = Button(self.master, text = "Search")
#    btn1.pack()

#root = Tk()
#root.title("Lazy Button 2")
#root.geometry("500x500")
#app = Application(root)
#root.mainloop()

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=int(os.environ.get('PORT', 8080)))
